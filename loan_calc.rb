puts "start"
interest = 0.08
years = 2.0
loan = 10.0**4
n = years * 12

i_for_n = interest / 12

installment = loan * ((i_for_n * ((1 + i_for_n)**n)) / (((1 + i_for_n)**n) - 1))


puts "loan: #{loan}"
puts "interest: #{interest}"
puts "years: #{years}"
puts "n: #{n}"
puts
puts "installment: #{"%.2f" % installment}"
puts "interest per perdiod: #{"%.4f" % (i_for_n * 100)}% (#{"%.2f" % (interest*100)}% / #{12})"
puts

curr_loan = loan
n.to_i.times do |i|
	curr_interest = curr_loan * i_for_n
	curr_principal = installment - curr_interest
	puts "[#{"%2i" % (i+1)}] #{"%8.2f"  % curr_loan}, \
#{"%.2f"  % (curr_interest + curr_principal)} = \
#{"%.2f" % curr_principal} + \
#{"%6.2f" % curr_interest} (#{"%.2f" % (i_for_n * 100)}% * #{"%9.2f" % curr_loan} )"
	curr_loan -= curr_principal
end

puts
puts "curr_loan: #{"%.2f" % curr_loan}"